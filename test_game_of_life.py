import pytest

from cell import DeadCell, LivingCell, NightOfLivingDeadException


def test_first():
    assert True

def test_dead_is_dead():
    dead_cell = DeadCell()

    new_cell = dead_cell.next_generation()

    assert new_cell == dead_cell

def test_live_and_let_died():
    alive_cell = LivingCell()

    new_cell = alive_cell.next_generation()

    assert isinstance(new_cell, DeadCell)

def test_dead_with_3_neighbours_becomes_alive():
    neighbours = [LivingCell() for _ in range(3)]
    dead_cell = DeadCell()
    new_cell = dead_cell.next_generation(neighbours)

    assert isinstance(new_cell, LivingCell)

def test_dead_with_2_neighbours_stays_dead():
    neighbours = [LivingCell() for _ in range(2)]
    dead_cell = DeadCell()
    new_cell = dead_cell.next_generation(neighbours)

    assert dead_cell == new_cell


def test_dead_with_dead_in_neighbours_raise_exception():
    neighbours = [DeadCell(), LivingCell()]
    dead_cell = DeadCell()
    with pytest.raises(NightOfLivingDeadException):
        dead_cell.next_generation(neighbours)

def test_life_is_life():
    neighbours = [LivingCell() for _ in range(2)]
    living_cell = LivingCell()
    new_cell = living_cell.next_generation(neighbours)

    assert living_cell == new_cell

def test_living_with_dead_in_neighbours_raise_exception():
    neighbours = [DeadCell(), LivingCell()]
    living_cell = LivingCell()
    with pytest.raises(NightOfLivingDeadException):
        living_cell.next_generation(neighbours)
