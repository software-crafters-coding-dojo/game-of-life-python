class Cell:
    def next_generation(self, neighbours: list = []):
        raise NotImplementedError()

    def _verify_input(self, neighbours):
        for neighbour in neighbours:
            if isinstance(neighbour, DeadCell):
                raise NightOfLivingDeadException()

class DeadCell(Cell):
    def next_generation(self, neighbours:list = []):
        self._verify_input(neighbours)
        if neighbours and len(neighbours) == 3:
            return LivingCell()
        return self

class LivingCell(Cell):
    def next_generation(self, neighbours:list = []):
        self._verify_input(neighbours)
        if neighbours and len(neighbours) == 2:
            return self
        return DeadCell()

class NightOfLivingDeadException(Exception):
    pass